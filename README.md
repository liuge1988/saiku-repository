# saiku-repository
There are saiku project needs jar.
這裡提供一些saiku執行mvn  package 命令時 从仓库中下载不下来的jar包

当前提供的saiku版本为 3.9

获取其他下载不下来jar的方式： 
	1. 下载最新版本的saiku-server ,从saiku-server\tomcat\webapps\saiku\WEB-INF\lib 目录下对应的jar包，copy到本地maven仓库中
	2. 从 https://nexus.pentaho.org/#browse/search  中搜索jar包，下载下来然后copy到本地maven仓库目录
	
	
mvn install:install-file -Dfile=mondrian-4.3.0.1.2-SPARK.jar -DgroupId=pentaho -DartifactId=mondrian -Dversion=4.3.0.1.2-SPARK -Dpackaging=jar
mvn install:install-file -Dfile=mondrian-3.11.0.0-353.jar -DgroupId=pentaho -DartifactId=mondrian -Dversion=3.11.0.0-353 -Dpackaging=jar
mvn install:install-file -Dfile=eigenbase-properties-1.1.0.10924.jar -DgroupId=eigenbase -DartifactId=eigenbase-properties -Dversion=1.1.0.10924 -Dpackaging=jar
mvn install:install-file -Dfile=eigenbase-resgen-1.3.0.11873.jar -DgroupId=eigenbase -DartifactId=eigenbase-resgen -Dversion=1.3.0.11873 -Dpackaging=jar
mvn install:install-file -Dfile=eigenbase-xom-1.3.0.11999.jar -DgroupId=eigenbase -DartifactId=eigenbase-xom -Dversion=1.3.0.11999 -Dpackaging=jar
mvn install:install-file -Dfile=saiku-query-0.4-SNAPSHOT.jar -DgroupId=org.saiku -DartifactId=saiku-query -Dversion=0.4-SNAPSHOT -Dpackaging=jar
mvn install:install-file -Dfile=licenseserver-core-1.0-SNAPSHOT.jar -DgroupId=bi.meteorite -DartifactId=licenseserver-core -Dversion=1.0-SNAPSHOT -Dpackaging=jar
mvn install:install-file -Dfile=iText-4.2.0.jar -DgroupId=iText -DartifactId=iText -Dversion=4.2.0 -Dpackaging=jar
mvn install:install-file -Dfile=miredot-annotations-1.3.1.jar -DgroupId=com.qmino -DartifactId=miredot-annotations -Dversion=1.3.1 -Dpackaging=jar
mvn install:install-file -Dfile=cpf-core-7.1.0.0-12.jar -DgroupId=pentaho -DartifactId=cpf-core -Dversion=7.1.0.0-12 -Dpackaging=jar
mvn install:install-file -Dfile=cpf-pentaho-7.1.0.0-12.jar -DgroupId=pentaho -DartifactId=cpf-pentaho -Dversion=7.1.0.0-12 -Dpackaging=jar
mvn install:install-file -Dfile=pentaho-platform-repository-7.1.0.0-12.jar -DgroupId=pentaho -DartifactId=pentaho-platform-repository -Dversion=7.1.0.0-12 -Dpackaging=jar
mvn install:install-file -Dfile=pentaho-platform-api-5.0.0.jar -DgroupId=pentaho -DartifactId=pentaho-platform-api -Dversion=5.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=pentaho-platform-core-5.0.0.jar -DgroupId=pentaho -DartifactId=pentaho-platform-core -Dversion=5.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=pentaho-platform-extensions-5.0.0.jar -DgroupId=pentaho -DartifactId=pentaho-platform-extensions -Dversion=5.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=mondrian-data-foodmart-hsql-0.1.jar -DgroupId=mondrian-data-foodmart-hsql -DartifactId=mondrian-data-foodmart-hsql -Dversion=0.1 -Dpackaging=jar